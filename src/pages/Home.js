import React from "react";
import Styles from "../stylesheets/Home.module.css";
export default function Home() {
  return (
    <div className={Styles.Home}>
      <h1>Home</h1>
    </div>
  );
}
